# auto_planner

Tool to help you find FLOSS software projects to contribute to.

# Research

* https://boyter.org/posts/an-informal-survey-of-10-million-github-bitbucket-gitlab-projects/
* [Shape Up](https://basecamp.com/shapeup)
* [Back Your Stack](https://backyourstack.com/)
* [Adopt OSS](https://adoptoposs.org/)
* [Where's the Yelp for open-source tools](https://www.functionize.com/blog/wheres-the-yelp-for-open-source-tools/)
  - links to various project analysis tools
* [xkcd:2347 Dependency](https://xkcd.com/2347/)
  - good picture out the parts of your infrastructure that auto_planner should
    guide you to support
* [Diataxis](https://diataxis.fr/) interesting documentation structure
* [Ice Panel](https://icepanel.io/) project diagraming tool
* https://www.harihareswara.net/sumana/2021/05/06/0
* [Fork Freshness](https://gilesbowkett.com/blog/2021/08/15/fork-freshness-project-lifespans-in-the-ruby-ecosystem/)
* [gems-status](https://github.com/jordimassaguerpla/gems-status)
* [OSSNutritionLabelPrototypes](https://github.com/IQTLabs/OSSNutritionLabelPrototypes)

## Discussions about unsupported components

* https://blog.filippo.io/professional-maintainers/
* https://christine.website/blog/open-source-broken-2021-12-11

## Game and prompt systems

* https://en.wikipedia.org/wiki/Alternate_reality_game
* https://en.m.wikipedia.org/wiki/Focal_point_(game_theory)
* https://en.m.wikipedia.org/wiki/Thomas_Schelling

# TODO
* review critical projects list from various places, like Linux Foundation of
  the FSF
